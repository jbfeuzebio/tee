import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {
  private data = [
    {nome: 'Aline', profissao: 'Design', idade: 32, icone: 'brush'},
    {nome: 'wallace', profissao: 'Design', idade: 29, icone: 'megaphone'},
  ];
  constructor() { }

  getData(){
    return this.data;
  }
}
