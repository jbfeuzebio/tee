import {LoginTopComponent} from './login-top/login-top.component'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [
    LoginTopComponent,
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    LoginTopComponent
  ]
})
export class ComponentModule { }
