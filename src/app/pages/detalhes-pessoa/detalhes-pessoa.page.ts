import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalhes-pessoa',
  templateUrl: './detalhes-pessoa.page.html',
  styleUrls: ['./detalhes-pessoa.page.scss'],
})
export class DetalhesPessoaPage implements OnInit {
  pessoa;
  
  constructor( private service: PessoaService) { }

  ngOnInit() {
    const str = localStorage.getItem('pessoa');
    this.pessoa = JSON.parse(str);
  }

}
