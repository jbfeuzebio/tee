import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalhesPessoaPageRoutingModule } from './detalhes-pessoa-routing.module';

import { DetalhesPessoaPage } from './detalhes-pessoa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalhesPessoaPageRoutingModule
  ],
  declarations: [DetalhesPessoaPage]
})
export class DetalhesPessoaPageModule {}
