import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetalhesPessoaPage } from './detalhes-pessoa.page';

const routes: Routes = [
  {
    path: '',
    component: DetalhesPessoaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetalhesPessoaPageRoutingModule {}
