import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  data;
  constructor(private service: PessoaService, private nav: NavController) {
    this.data = service.getData()
  }
  detalhes(pessoa){
    const obj = JSON.stringify(pessoa);
    localStorage.setItem('pessoa', obj);
    this.nav.navigateForward('detalhe-pessoa');
  }
}
