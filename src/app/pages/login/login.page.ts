import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email;
  senha;
  //injeção de dependencia 
  constructor(private nav: NavController) {
    
   }

  login(){
    if(this.email == 'maria' && this.senha == '123'){  
      console.log('login realizado');
      this.nav.navigateForward('home');
    }else {
      console.log('deu merda');
    }
  }
  
  esqueci(){
    this.nav.navigateForward('forgot');
  }

  ngOnInit() {
  }

}
